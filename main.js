
var myGamePiece;
var myObstacle;
var myObstacle1;
var myObstacle2;
var myObstacle3;
var myRoad;
var myRoad1;
var myRoad2;
var myRoad3;
var myScore;
var myHighScore;
var myLevel;
var myGameOver;
var mySound;


function startGame() {
    
	myRoad = new component(100,400,"road.png", 150, 0,"image");
	myRoad1 = new component(100,400,"road.png", 150, 125,"image");
	myRoad2 = new component(100,400,"road.png", 150, 250,"image");
	myRoad3= new component(100,400,"road.png", 150, 375,"image");
	myGamePiece = new component(35,60,"newPlayer1.png", 200, 435,"image");
	myObstacle = new component(35,60, "1.png", Math.floor((Math.random()*30)+1), Math.floor((Math.random()*5)+1),"image");
	myObstacle1 = new component(35,60, "1.png", Math.floor((Math.random()*100)+90), Math.floor((Math.random()*15)+10),"image");
	myObstacle2 = new component(35,60, "1.png", Math.floor((Math.random()*160)+145), Math.floor((Math.random()*30)+20),"image");
	myObstacle3 = new component(35,60, "1.png", 345, Math.floor((Math.random()*50)+30),"image");
	myScore = new component("15px", "Consolas", "white", 100, 40, "text");
	myLevel = new component("15px", "Consolas", "white", 10, 40, "text");
	mySound = new sound("crash.mp3");
	myGameArea.start();
}

function component(width, height, color, x, y, type) {
	this.type = type;
	if (type == "image") {
    this.image = new Image();
    this.image.src = color;
	}
    this.width = width;
    this.height = height;
	this.speedX = 0;
    this.speedY = 0;
    this.x = x;
    this.y = y; 
	this.update = function(){
		ctx = myGameArea.context;
		if (this.type == "text") {
		  ctx.font = this.width + " " + this.height;
		  ctx.fillStyle = color;
		  ctx.fillText(this.text, this.x, this.y);
		  }
		if (type == "image") {
			ctx.drawImage(this.image, this.x, this.y,this.width, this.height);
		}
		else{
			ctx.fillStyle = color;
			ctx.fillRect(this.x, this.y, this.width, this.height);
		}
	}
	this.newPos = function() {
        this.x += this.speedX;
        //this.y += this.speedY; 
    }
	this.crashWith = function(otherobj) {
        var myleft = this.x;
        var myright = this.x + (this.width);
        var mytop = this.y;
        var mybottom = this.y + (this.height);
        var otherleft = otherobj.x;
        var otherright = otherobj.x + (otherobj.width);
        var othertop = otherobj.y;
        var otherbottom = otherobj.y + (otherobj.height);
        var crash = true;
        if ((mybottom < othertop) ||
               (mytop > otherbottom) ||
               (myright < otherleft) ||
               (myleft > otherright)) {
           crash = false;
        }
        return crash;
		
    }
	
}
function sound(src) {
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);
    this.play = function(){
        this.sound.play();
    }
    this.stop = function(){
        this.sound.pause();
    }
}
function updateGameArea() {
    myGameArea.clear();
	myRoad.update();
	myRoad1.update();
	myRoad2.update();
	myRoad3.update();
	myObstacle.update();
	myObstacle1.update();
	myObstacle2.update();
	myObstacle3.update();
	myGamePiece.speedX = 0;
	allMovements();
	
	//SPEED LEVEL
	if(myGameArea.frameNo >= 25){
		myObstacle.y += 3.8;
		myObstacle1.y += 3.8;
		myObstacle2.y += 3.8;
		myObstacle3.y += 3.8;
		myRoad.y += 3.8;
		myRoad1.y += 3.8;
		myRoad2.y += 3.8;
		myRoad3.y += 3.8;
		myObstacle.image.src = "2.png";
		myObstacle1.image.src = "2.png";
		myObstacle2.image.src = "2.png";
		myObstacle3.image.src = "2.png";
	}
	if(myGameArea.frameNo >= 50){
		myObstacle.y += 3.9;
		myObstacle1.y += 3.9;
		myObstacle2.y += 3.9;
		myObstacle3.y += 3.9;
		myRoad.y += 3.9;
		myRoad1.y += 3.9;
		myRoad2.y += 3.9;
		myRoad3.y += 3.9;
		myObstacle.image.src = "3.png";
		myObstacle1.image.src = "3.png";
		myObstacle2.image.src = "3.png";
		myObstacle3.image.src = "3.png";
	}
	if(myGameArea.frameNo >= 100){
		myObstacle.y += 4;
		myObstacle1.y += 4;
		myObstacle2.y += 4;
		myObstacle3.y += 4;
		myRoad.y += 4;
		myRoad1.y += 4;
		myRoad2.y += 4;
		myRoad3.y += 4;
		myObstacle.image.src = "4.png";
		myObstacle1.image.src = "4.png";
		myObstacle2.image.src = "4.png";
		myObstacle3.image.src = "4.png";
	}

//SPEED OF myGamePiece
var speedLeft= -5;
var speedRight= 5;
	if (myGameArea.frameNo >=25){
		speedLeft = -8;
		speedRight = 8;
	}
	if (myGameArea.frameNo >=50 ){
		speedLeft = -10;
		speedRight = 10;
	}
	if (myGameArea.frameNo >=100 ){
		speedLeft = -15;
		speedRight = 15;
	}

//CONTROLLER
    if (myGameArea.key && myGameArea.key == 37 ) {//KEYCODE
		myGamePiece.speedX = speedLeft;
		if(myGamePiece.x < 4){
			myGamePiece.x = 4;//avoid glitch to the left side
		}
		}
    if (myGameArea.key && myGameArea.key == 39 ) {//KEYCODE
		myGamePiece.speedX = speedRight; 
			if(myGamePiece.x > 345){
			myGamePiece.x = 345;//avoid glitch in the right side
		}
		}
		
	myScore.text="SCORE: " + myGameArea.frameNo;//Scoring
    myScore.update();
	if (myGameArea.frameNo ==25){
		myGameArea.levelNo = 1;
	}
	if (myGameArea.frameNo ==50){
		myGameArea.levelNo = 2;
	}
	if (myGameArea.frameNo ==100){
		myGameArea.levelNo = 3;
	}		
	myLevel.text="LEVEL: " + myGameArea.levelNo;
    myLevel.update();
	myGamePiece.newPos();
    myGamePiece.update();
}
function allMovements(){
	myObstacle.y += 3.5;
		if(myObstacle.y >= 480){
			myObstacle.y=Math.floor((Math.random()*5)+1);
			myObstacle.x=Math.floor((Math.random()*30)+1);
			myGameArea.frameNo += 1;
			
			
		}
		if(myObstacle.crashWith(myGamePiece)){
			myGameArea.clear();
			myGameArea.stop();//STOP GAME..
			myGamePiece.width=0;//to remove after clearing
			mySound.play();
		}
	myObstacle1.y += 3.5;
		if(myObstacle1.y >=480){
			myObstacle1.y=Math.floor((Math.random()*15)+10);
			myObstacle1.x=Math.floor((Math.random()*100)+90);
			myGameArea.frameNo += 1;
			
		}
		if(myObstacle1.crashWith(myGamePiece)){
			//myObstacle1.y += 400;
			myGameArea.clear();
			myGameArea.stop();//STOP GAME..
			myGamePiece.width=0;//to remove after clearing
			mySound.play();
			
		}
	myObstacle2.y += 3.5;
		if(myObstacle2.y >=480){
			myObstacle2.y=Math.floor((Math.random()*30)+20);
			myObstacle2.x=Math.floor((Math.random()*160)+145);
			myGameArea.frameNo += 1;
			
		}
		if(myObstacle2.crashWith(myGamePiece)){
			myGameArea.clear();
			myGameArea.stop();//STOP GAME..
			myGamePiece.width=0;//to remove after clearing
			mySound.play();
			
		}
	myObstacle3.y += 3.5;
		if(myObstacle3.y >=480){
			myObstacle3.y=Math.floor((Math.random()*50)+30);
			myObstacle3.x=345;
			myGameArea.frameNo += 1;
			
		}
		if(myObstacle3.crashWith(myGamePiece)){
			myGameArea.clear();
			myGameArea.stop();//STOP GAME..
			myGamePiece.width=0;//to remove after clearing
			mySound.play();
			
		}
		if(myObstacle1.crashWith(myObstacle2)){//to avoid collision
			myObstacle2.y += 50;
		}
//ROAD ANIMATION
	myRoad.y += 3;
		if (myRoad.y>480){
			myRoad.y=0;
		}
	myRoad1.y += 3;
		if (myRoad1.y>480){
			myRoad1.y=0;
		}
	myRoad2.y += 3;
		if (myRoad2.y>480){
			myRoad2.y=0;
		}
	myRoad3.y += 3;
		if (myRoad3.y>480){
			myRoad3.y=0;
		}		
}

var myGameArea = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.width = 400;
        this.canvas.height = 500;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);//iinsert niya ang document.body ang canvas sa childNodes[0]
		this.frameNo = 0;
		this.levelNo = 0;
		this.highScore= localStorage.getItem('number');
		this.interval = setInterval(updateGameArea, 20); //iupdate niya ang updateGameArea every 20secs
		document.getElementById("start").style.visibility="hidden";
		window.addEventListener('keydown', function (e) {//key is pressed ('keydown')
            myGameArea.key = e.keyCode;
        })
        window.addEventListener('keyup', function (e) { //When the key is released, set the key property to false: ('keyup')
            myGameArea.key = false;
        })
	},
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height); //clear niya ang context which is atong canvas
    },
	stop : function() {
        clearInterval(this.interval); //iistop niya ang pagtawag sa updateGameArea every 20secs
		myGameOver = new component("60px", "Consolas", "white", 50, 250, "text");
		myGameOver.text = "GAME OVER";
		myHighScore = new component("15px", "Consolas", "white", 130, 280, "text");
		//console.log(typeof myGameArea.frameNo); number type
		if (myGameArea.highScore<myGameArea.frameNo){
			myGameArea.highScore = myGameArea.frameNo;
			localStorage.setItem('number',myGameArea.highScore);
			var highestScore = localStorage.getItem('number');
		}else{
			var highestScore = localStorage.getItem('number');
		}
		//console.log(typeof highestScore); number type
		//localStorage.removeItem('number');
		myHighScore.text="HIGHEST SCORE: " + highestScore;
		console.log(myGameArea.highScore);
		myHighScore.update();
		myGameOver.update();
		
		document.getElementById("start").style.visibility="visible";
    }
}

	function restart() {
		//myGameArea.stop();
		//myGameArea.clear();
		startGame();
	}
	
/*
The HTML5 <canvas> tag is used to draw graphics, on the fly, via scripting (usually JavaScript).
However, the <canvas> element has no drawing abilities of its own (it is only a container for graphics) - you must use a script to actually draw the graphics.
The getContext() method returns an object that provides methods and properties for drawing on the canvas.
This reference will cover the properties and methods of the getContext("2d") object, which can be used to draw text, lines, boxes, circles, and more - on the canvas.
*/
